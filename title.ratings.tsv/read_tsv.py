import pandas as pd 

chunksize = 2000000
chunks = []
get_chunks = []
for chunk in pd.read_table('data.tsv', chunksize=chunksize):
    new_df = chunk[chunk["tconst"] == "tt0944947"]
    get_chunks.append(new_df)
    #print(chunk.loc[chunk["primaryTitle"] == "Game of Thrones", ("tconst")].where(chunk["titleType"] == "tvSeries"))
    # chunks.append(chunk)

another_df = pd.concat(get_chunks, axis = 0)
print(another_df.head())
print("\nNew DataFrame\n")
print(another_df)

# for season_Number in another_df["seasonNumber"]
# dropped_df = another_df.dropna()
# print("\nDROPPED TABLE\n")
# print(dropped_df.head())

# tconst_value = dropped_df.iloc[0,0]
# print("THIS IS IT",tconst_value)

# df = pd.concat(chunks, axis=0)

# print(df.head())

#https://cmdlinetips.com/2018/01/how-to-load-a-massive-file-as-small-chunks-in-pandas/

# tt0944947 Game of Thrones