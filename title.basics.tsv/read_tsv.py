import pandas as pd 

print("Enter the series Name")
seriesName = input()

chunksize = 2000000
chunks = []
get_chunks = []
for chunk in pd.read_table('data.tsv', chunksize=chunksize):
    new_df = chunk[chunk["primaryTitle"] == seriesName].where(chunk["titleType"] == "tvSeries")
    get_chunks.append(new_df)
    #print(chunk.loc[chunk["primaryTitle"] == "Game of Thrones", ("tconst")].where(chunk["titleType"] == "tvSeries"))
    # chunks.append(chunk)

another_df = pd.concat(get_chunks, axis = 0)
print(another_df.head())
dropped_df = another_df.dropna()
print("\nDROPPED TABLE\n")
print(dropped_df.head())

tconst_value = dropped_df.iloc[0,0]
print("THIS IS IT",tconst_value)

#
#Extensions
#

chunksize = 2000000
chunks = []
get_chunks = []
for chunk in pd.read_table('dataEpisode.tsv', chunksize=chunksize):
    new_df = chunk[chunk["parentTconst"] == tconst_value]
    get_chunks.append(new_df)
    #print(chunk.loc[chunk["primaryTitle"] == "Game of Thrones", ("tconst")].where(chunk["titleType"] == "tvSeries"))
    # chunks.append(chunk)

episode_df = pd.concat(get_chunks, axis = 0)
# print(episode_df.head())
# print("\nNew DataFrame\n")
print(episode_df)

episode_df[["seasonNumber","episodeNumber"]] = episode_df[["seasonNumber","episodeNumber"]].apply(pd.to_numeric)

# grouped_df = episode_df.groupby(["seasonNumber", "episodeNumber"])

# print("THIS IS IT")
# print(grouped_df.first())

chunksize_1 = 2000000
chunks_1 = []
get_chunks_1 = []
i=0

for chunk in pd.read_table('dataRating.tsv', chunksize=chunksize_1):
    get_chunks_1.append(chunk)

rating_df = pd.concat(get_chunks_1, axis=0)
# print(rating_df.head())

result_df = pd.merge(episode_df,rating_df, how='left', on='tconst')

# print(result_df.tail())

grouped_df = result_df.groupby(["seasonNumber", "episodeNumber"])

# print("THIS IS IT")
print(grouped_df.first())

# df = pd.concat(chunks, axis=0)

# print(df.head())

#https://cmdlinetips.com/2018/01/how-to-load-a-massive-file-as-small-chunks-in-pandas/

import matplotlib.pyplot as plt

plt.plot(grouped_df['averageRating'])
plt.show()