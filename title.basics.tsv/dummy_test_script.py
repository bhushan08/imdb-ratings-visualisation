import pandas as pd

chunk_df_list = []
for chunk_df in pd.read_table('dummy_test.tsv', chunksize=50):
    print(chunk_df[chunk_df["primaryTitle"] == "Blacksmith Scene"])
    chunk_df_list.append(chunk_df)

df = pd.concat(chunk_df_list, axis=0)
